/* top struct blueprint

Runner {
    Runner{
        TRunner
    }
    

    RunnerContext
}


const (
    udp,
    tcp
    )

Runner<<CONF>> as RunnerController {
    // 頂級結構Runner只關心以下幾項功能
    // tcp和udp基本設定

    // 用來分配runner內id的原子值
    runner_uuid:RunnerUUID

    // RPC id
    runner_id:u32,
    runner_name:String,
    runner_conf:RunnerConf{
        tcp_addr:String
        udp_addr:String
    }
    conf_customized:CONF


    runner_impl:TRunnerImpl{
        impl fn init(Runner)
        impl fn shutdown(Runner)
        impl fn handle(RunnetCTX)
    }

    fn (Runner)Run()

    // 讀取u8還原整個runner
    fn (Runner)load([]u8)result<(),stdError>
    fn (Runner)load_from_file(path)result<(),stdError>

    // 將整個runner存檔成[]u8
    fn (Runner)save()result<[]u8,stdError>
    // 存檔成指定檔案
    fn (Runner)save_as_file(path)<(),stdError>

    // runner_remote 用於呼叫其他runner 晚點才會實現 先實現單台機多服務
    // (Runner)runner_handle(TRunnerCTX)

    // 增量輸出
    fn (Runner)register()RunnerUUID
    fn (Runner)deregister(RunnerUUID)


    conntection_pool:map[ConnectionID]ConnectionContext{
        fn (ConnectionContext)udp_send(data:Any)result<(),()>
        fn (ConnectionContext)tcp_send(data:Any)result<(),()>
        fn (ConnectionContext)close(connection)
        fn (ConnectionContext)join_chat()result<ChatController,()>
        ctx_revice:Revice
        ctx_sender:Sender
    }
    fn (Runner)supper_brodcast_udp([]u8)
    fn (Runner)supper_brodcast_tcp([]u8)

    chats_manager:ChatsGroupManager{
        *fn chat_create()ChatController
        *fn chat_delete()ChatController
        *fn chat_get(ChatID)ChatController
        chats:map[ChatID]ChatPod as ChatController{
            *fn close()
            *fn id()ChatID
            *fn join(ConnectionContext)
            *fn udp_sned([]byte)result<(),stdError>
            *fn tcp_sned([]byte)result<(),stdError>

            chat_id:ChatID
            subscriber:map[ConnectionID]Sender
        }
    }

    fn (Runner)service_create(Service)result<Service,strError{

    }
    fn (Runner)service_shotdow()

    services:map[ServiceID]Service {
        ruid:RunnerUUID
        service_impl:TServiceImpl{
            impl *fn start(ServiceCTX)
            impl *fn handle(ServiceCTX)
            impl *fn shutdown(ServiceCTX)
        }



        scenes:map[ScenesID]Scenes{
            *fn create_gameobject(TGameObject)

            *fn game_object_create(TGameObject)TGameObject
            *fn game_object_delete(TGameObject)
            TGameObject{
                start:fn(ObjectCTX<T>)
                frame:fn(ObjectCTX<T>)
                final:fn(ObjectCTX<T>)
            }


            // scenes_impl 為none的時候代表這個service為none frame service

            game_object:map[GameObjectID]TGameObject<T>{
                ruuid:RunnerUUID
                start:fn(ObjectCTX<T>)
                frame:fn(ObjectCTX<T>)
                final:fn(ObjectCTX<T>)
                ObjectCTX<T>{
                    *fn status()T
                    *fn service()
                }
            }
        }
    }
}

TChat{
    fn id()ChatID
    fn udp_send()result<(),stdError>
    fn tcp_end()result<(),stdError>
    fn join(TConnectionCTX)
    fn leave(TConnectionCTX)
}

TConnection {
    fn udp_send()
    fn tcp_send()

}

TRunnerCTX{
   fn (TRunnerCTX)conf()CONF
   fn (TRunnerCTX)service_create(TServiceImpl)ServiceCTX
   fn (TRunnerCTX)service_delete(ServiceID)result<(),strError>
   fn (TRunnerCTX)service_get(ServiceID)ServiceCTX
   fn (TRunnerCTX)chat_create()TChat

}

TServiceCTX{
    fn runner()TRunnerCTX
    fn conf()CONF
    // fn game_object()
}

TGameOjbectCTX{
    fn cmd(CMD)result<(),stdError>
}

struct expg{
    frame(objctx){
        match objctx.cmd.cmdtpye {
            roll_dice => {
                let a = rand.inti();
                objctx.scene.status().referee().let_me_push(a);
            },
        }
    }
}





*/
