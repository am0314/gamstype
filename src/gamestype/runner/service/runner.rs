use std::{
    any::Any,
    collections::HashMap,
    convert::TryInto,
    fmt::Error,
    rc::Rc,
    sync::{Arc, Mutex, RwLock},
};

use evmap;
use futures::{executor::block_on, io::Copy};

use tokio::{
    io::{AsyncReadExt, AsyncWriteExt},
    net::{TcpListener, UdpSocket},
};

use crate::gamestype::{
    connection::{
        context::BContext,
        entity::entity::{Entity, TEntityRC},
    },
    logger::logger::TLogger,
    runner::scene::{
        scenes::{Scenes, ScenesSet, TScenes},
        scenes_controller::TScenesController,
    },
};

use super::{
    conf::ListenConfig,
    default_serialize::{DefaultDeSerialize, DefaultSerialize},
};
// use super::{conf::{ListenConfig, BasicConfigFill}, default_serialize::{BDefaultSerialize, BDefaultDeSerialize}};

pub type StageResult = Result<(), String>;
pub type GenericID = u64;

/// 最頂級結構體
/// CONFSET
/// CONNCTX
/// REVICEAC 用來表示所有接收體的集合 會在make_rouate完成註冊
pub struct Runner<CONFSET, CONNCTX, REVICEAC>
where
    REVICEAC: Clone,
{
    /// 連線池
    pub conn_pool: HashMap<std::net::SocketAddr, Arc<dyn TEntityRC>>,
    /// 基本設定
    pub set: RunnerSetting<CONFSET>,
    // pub HeaderMatch:HashMap<u32,&'a BOUService<REVICEAC>>
    // cmd_entity_ctx_match 用來匹配cmd應該使用哪一個ctx以及進入對應的service
    pub cmd_entity_ctx_match: Vec<usize>,

    // pub ctx_template: Vec<BContext<CONFSET, CONNCTX, REVICEAC>>,
    /// 通用ID會發給每個註冊於runner底下的一切物件 scene object connection
    pub generic_id: GenericID,

    /// 序列化 反序列化
    pub serialize: Box<dyn Fn(&dyn Any) -> Vec<u8>>,
    pub deserialize: Box<dyn Fn(&dyn Any, Vec<u8>)>,

    /// 由Runner所管理的scenes
    pub scenes: Vec<Arc<dyn TScenes>>,
}

impl<CONFSET, CONNCTX, REVICEAC, SCENE, GAMEOBJECT> Runner<CONFSET, CONNCTX, REVICEAC>
where
    SCENE: TScenes,
    REVICEAC: Clone,
    CONNCTX: Clone,
{
    /// 分配一個id
    pub fn allotment_id(&mut self) -> u64 {
        self.generic_id += 1
    }

    /// 使用RunnerSetting設定runner的方法
    pub fn new(set: RunnerSetting<CONFSET>) -> Self {
        let pool = HashMap::new();
        let (serialize, deserialize) = match set.serialize {
            Some(serializes) => (serializes.0, serializes.1),
            None => (Box::new(DefaultSerialize), Box::new(DefaultDeSerialize)),
        };
        Runner {
            conn_pool: pool,
            set,
            cmd_entity_ctx_match: Vec::new(),
            // ctx_template: Vec::new(),
            generic_id: 0,
            serialize,
            deserialize,
            scenes: Vec::new(),
        }
    }

    pub fn new_scene(
        &mut self,
        scene_entity: ScenesSet<SCENE>,
    ) -> Box<dyn TScenesController<SCENE, GAMEOBJECT>> {
        let ns = Scenes::new(scene_entity, self.serialize, self.deserialize);
        ns.scene_obj().OnStart();

        tokio::task::spawn(async {
            ns.scenes_run();
        });

        return ns;
    }

    pub fn pool_inster(&mut self, k: std::net::SocketAddr, v: tokio::net::TcpStream) {
        // let e = Entity::new_tcp_conn(
        //     k,
        //     v,

        // );
        // let e = Arc::new(RwLock::new(e));
    }
}

// ServiceContainer Initialize the main parameters struct
pub struct RunnerSetting<CONFSET> {
    // service basic config
    pub basic_config: ListenConfig,

    // /// basic_config_fill , if not none , will run after server start() befor handle()
    // pub basic_config_fill: Option<BasicConfigFill>,

    // cconfig is option , your customized configuration
    pub customized_config: CONFSET,

    /// customized_config_fill , if not none , will run after server start() befor handle()
    // pub customized_config_fill: Option<fn(&mut T)>,

    // cconfigfile is option field , If you want to fill in a customized configuration file yourself (for example from etcd shared configuration)
    // and , remember to set the mut value  , the fn well trigger befor start() end
    // pub customized_config_filler: fn(T),

    // 序列化 反序列化方法 預設使用json
    pub serialize: Option<Box<dyn Fn(&dyn Any) -> Vec<u8>>>,
    pub deserialize: Option<Box<dyn Fn(&dyn Any, Vec<u8>)>>,
    // pub Deserialize:Option<()>,
    pub logger: Option<Box<dyn TLogger>>,
}
