use bytes::Bytes;
use futures::{Sink, SinkExt, Stream, StreamExt};
use std::error::Error;
use std::io;
use std::net::SocketAddr;
use tokio::net::UdpSocket;

pub async fn connect(
    addr: &SocketAddr,
    stdin: impl Stream<Item = Result<Bytes, io::Error>> + Unpin,
    stdout: impl Sink<Bytes, Error = io::Error> + Unpin,
) -> Result<(), Box<dyn Error>> {
    // We'll bind our UDP socket to a local IP/port, but for now we
    // basically let the OS pick both of those.
    let bind_addr = if addr.ip().is_ipv4() {
        "0.0.0.0:0"
    } else {
        "[::]:0"
    };

    let socket = UdpSocket::bind(&bind_addr).await?;
    socket.connect(addr).await?;

    tokio::try_join!(send(stdin, &socket), recv(stdout, &socket))?;

    Ok(())
}

async fn send(
    mut stdin: impl Stream<Item = Result<Bytes, io::Error>> + Unpin,
    writer: &UdpSocket,
) -> Result<(), io::Error> {
    while let Some(item) = stdin.next().await {
        let buf = item?;
        writer.send(&buf[..]).await?;
    }

    Ok(())
}

async fn recv(
    mut stdout: impl Sink<Bytes, Error = io::Error> + Unpin,
    reader: &UdpSocket,
) -> Result<(), io::Error> {
    loop {
        let mut buf = vec![0; 1024];
        let n = reader.recv(&mut buf[..]).await?;

        if n > 0 {
            stdout.send(Bytes::from(buf)).await?;
        }
    }
}
