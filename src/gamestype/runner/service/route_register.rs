#[macro_export]
macro_rules! make_context_routes {
    (
        // $runner_conf:ident
        $runner:ident
        $entity_context_ident:ident
        $(fn $method:ident (&mut $self:ident, $ctx:ident , $param_ident:tt : &$param_type:tt) { $($code:tt)* })*
    ) => {
        use lazy_static::lazy_static;
        use std::collections::HashMap;
        use crate::gamestype::{connection::context::*};
        use std::any::Any;
        use std::net::IpAddr;
        use crate::Runner;
        use crate::gamestype::runner::scene::scenes_controller::ScenesController;
        use crate::gamestype::connection::context::Subscription;
        use crate::gamestype::connection::entity::entity::EntityCTX;
        use crate::gamestype::logger::logger::ALogger;

        // 捕獲所有handle
        $(
            const $method : u8 = EMethod::$method as u8;
        )*

        // 列舉所有方法集合 用於路由說明
        enum EMethod {
            $($method ,)*
        }


        impl $runner
        where $entity_context_ident:EntityCTX
        {
            $(
                fn $method (&mut $self, $ctx: BOverupContext<$runner,$entity_context_ident, ReviceProtocalAC> , $param_ident: &$param_type) { $($code)* }
            )*
        }

        // // 給service_ident再包一層來裝offset
        // struct OverupService<SETCONF, CONNCTX, REVICEAC> {
        //     conf:SETCONF,
        //     enctx:CONNCTX,
        //     rac:REVICEAC,
        //     offset:u8,
        // }

        struct OverupContext<SETCONF, CONNCTX, REVICEAC> {
            c:BContext<SETCONF, CONNCTX, REVICEAC>,
        }
        impl <SETCONF, CONNCTX, REVICEAC>TOverupContext<SETCONF, CONNCTX, REVICEAC>
        for OverupContext<SETCONF, CONNCTX, REVICEAC>
        where REVICEAC:Clone
        {
            fn entity(&mut self) -> CONNCTX{
                self.c.sub()
            }

            fn revice_ac(&mut self)->REVICEAC{
                self.c.revice_ac()
            }
            fn logger(&mut self) -> ALogger{
                self.c.logger()
            }
            fn addr(&mut self) -> IpAddr{
                self.c.addr()
            }
            fn tcp_send(&mut self, data: Box<dyn Any>){
                self.c.tcp_send(data)
            }
            fn udp_send(&mut self, data: Box<dyn Any>){
                self.c.udp_send(data)
            }
            fn conn_close(&mut self){
                self.c.conn_close()
            }
            fn scenes_subscribed(&mut self,scenes:ScenesController){

            }
            fn scenes_unsubscribed(&mut self,scenes:ScenesController){
                
            }

            fn chan_make(&mut self, s: Subscription){
                self.c.chan_make(s)
            }
            fn chan_join(&mut self, s: Subscription){
                self.c.chan_join(s)
            }
            fn chan_close(&mut self, s: Subscription){
                self.c.chan_close(s)
            }

            // subscribed show list of already subscribed chan
            fn chan_subscribed_list(&mut self){
                self.c.chan_subscribed_list()
            }

            // subscribeable_list list
            fn chan_subscribeable_list(&mut self){
                self.c.chan_subscribeable_list()
            }
            fn chan_subscription(&mut self, s: Subscription){
                self.c.chan_subscription(s)
            }
            fn chan_unsubscribe(&mut self, s: Subscription){
                self.c.chan_unsubscribe(s)
            }

            // overup
            fn serialization_self(&mut self)->Result<(),&str>{
                todo!()
            }
        }

        // impl <SETCONF, CONNCTX, REVICEAC> OverupService <SETCONF, CONNCTX, REVICEAC> {
        //     $(
        //         fn $method (&mut $self, $ctx: &BContext<$runner_conf, $entity_context_ident, ReviceProtocalAC> , $param_ident: &$param_type)
        //         {
        //             $($code)*
        //         }
        //     )*
        //     fn CalculateOffset(&self)->u8{
        //         let offset: u8=0;
        //         $(
        //             if offset < EMethod::$method as u8{
        //                 offset= EMethod::$method as u8
        //             }
        //         )*
        //         offset
        //     }
        // }

        // impl <SETCONF, CONNCTX, REVICEAC>OverupTService< SETCONF, CONNCTX, REVICEAC>
        // for
        // OverupService<SETCONF, CONNCTX, REVICEAC>
        // where REVICEAC:Clone
        // {
        //     fn start(&mut self){
        //         self.service_impl.start();
        //     }
        //     fn shotdown(&mut self){
        //         self.service_impl.shotdown();
        //     }

        //     fn offset(&mut self)->u8{
        //         self.offset
        //     }
        //     fn set_offset(&mut self,offset:u8){
        //         self.offset=offset;
        //     }


        //     fn call_fn(&mut self,cmd:Vec<u8>,ctx:&dyn Any){
        //         if let Some(myctx) = ctx.downcast_ref::<BContext<$runner_conf, $entity_context_ident, ReviceProtocalAC>>() {
        //             match cmd[0] {
        //                 $(
        //                     $method => {
        //                         myctx.runner().serialize_deserialize.1(&myctx.revice_ac().$method,cmd);
        //                         self.$method(myctx,&myctx.revice_ac().$method);
        //                     },
        //                 )*
        //             }
        //         }
        //     }
        // }




        // server通訊資料異體結構器
        // 異體結構器 每一個context都要包含一個，佔用較多記憶體空間，但避免競爭
        #[derive(Default)]
        pub struct ReviceProtocalAC {
            $($method:$param_type,)*
        }
        impl Clone for ReviceProtocalAC {
            fn clone(&self) -> Self {
                Self{
                    $($method:self.$method.clone(),)*
                }
            }
        }
        // 建構路由對照表結構
        struct HandleRoute {
            list:HashMap<u8,String>,
        }
        impl HandleRoute{
            fn handles_list(&self)->&HashMap<u8,String>{
                &WorkAC.list
            }
        }

        lazy_static! {
            // 暫時使用共用AC
            static ref AC:ReviceProtocalAC=ReviceProtocalAC{
                $($method:Default::default(),)*
            };
            // 建立工作路由對照表
            static ref WorkAC:HandleRoute = HandleRoute{
                list:{
                    let mut h = HashMap::new();
                    $(
                        h.insert(EMethod::$method as u8,stringify!($method).to_string());
                    )*
                    h
                },
            };
        }
    };
}
