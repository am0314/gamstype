pub struct ListenConfig {
    listens: Vec<Listen::ListenSetting>,
}
impl ListenConfig {
    ///
    /// # Examples
    /// ```
    /// let settings = vec![Listen::kcp("0.0.0.0:5959")];
    /// let nbc = ListenConfig::new(settings);
    /// ```
    /// or
    /// ```
    /// let nbc = ListenConfig::new(vec![Listen::kcp("0.0.0.0:5959")]);
    /// ```
    pub fn new(set: Vec<Listen::ListenSetting>) -> Self {
        Self { listens: set }
    }
}
pub mod Listen {
    /// # Examples
    ///  ```
    /// tcp("0.0.0.0:21655")
    /// tcp(":21655")
    /// ```
    pub fn tcp(addr: String) -> ListenSetting {
        return Listener {
            addr,
            listen_type: ListeneType::TCP,
        };
    }
    /// # Examples
    ///  ```
    /// udp("0.0.0.0:21655")
    /// udp(":21655")
    /// ```
    pub fn udp(addr: String) -> ListenSetting {
        return Listener {
            addr,
            listen_type: ListeneType::UDP,
        };
    }
    /// # Examples
    ///  ```
    /// kcp("0.0.0.0:21655")
    /// kcp(":21655")
    /// ```
    pub fn kcp(addr: String) -> ListenSetting {
        return Listener {
            addr,
            listen_type: ListeneType::KCP,
        };
    }
    pub trait ListenSetting {
        fn Addr(&self) -> String;
    }
    pub enum ListeneType {
        TCP,
        UDP,
        KCP,
    }
    struct Listener {
        addr: String,
        listen_type: ListeneType,
    }
}






// pub type BasicConfigFill = fn(&mut ListenConfig);
