use std::{any::Any, sync::Arc};

use crossbeam::channel::Sender;


use super::{game_object_controller::TGameObjectController, scenes::{Scenes, FrameNum}};

pub type AScenesController<SCENE, GAMEOBJECT> = Arc<dyn TScenesController<SCENE, GAMEOBJECT>>;

pub type BScenesController<SCENE, GAMEOBJECT> = Box<dyn TScenesController<SCENE, GAMEOBJECT>>;
/// TScenesController 是曝露出去使用的特徵 S代表場景體 G代表遊戲物件
/// 當開發者對runer請求一個場景時 我們會需要開發者實做場景啟動與關閉的邏輯
/// 場景的生命週期為  OnStart  OnFrame(內部循環) OnFinal
/// 在整個OnFrame中 場景依舊需要接收外往內的操作 同時內部需要不斷循環幀邏輯
/// 因此無論是外部接收工作 還是內部幀循環 都會通過TScenesController完成 而不是直接對場景物件
/// 亦即TScenesController即為整個SCENES的上下文
pub trait TScenesController<SCENE, GAMEOBJECT> {
    // create_gameobject 透過Scenes建立物件 目的是將該物件幀邏輯註冊在Scenes中
    fn create_gameobject(
        &mut self,
        obj: GAMEOBJECT,
    ) -> Box<dyn TGameObjectController<SCENE, GAMEOBJECT>>;
    fn remove_gameobject(&mut self);
    // mean 取得幀除數
    fn mean(&self) -> f64;
    /// meanc 計算輸入的幀除數 例如每秒10幀 動量爲20 則f=f*mean
    /// 如果用於計時 應不斷跌加mean
    fn meanc(&self, f: &f64) -> f64;

    ///設定server幀數
    fn frame_pres_set(&mut self,num:FrameNum);

    fn tcp_send(&mut self, conn_id: ConnectionID, data: dyn Any);
    fn udp_send(&mut self, conn_id: ConnectionID, data: dyn Any);
    fn kcp_send(&mut self, conn_id: ConnectionID, data: dyn Any);

    /// 立即對scenes廣播 採用TCP(如果有建立TCP連線的話)
    fn tcp_broadcast(&mut self, data: dyn Any);
    /// 立即對scenes廣播 採用UDP(如果有建立UDP連線的話)
    fn udp_broadcast(&mut self, data: dyn Any);
    /// 立即對scenes廣播 採用KCP(如果有建立KCP連線的話)
    fn kcp_broadcast(&mut self, data: dyn Any);

    /// 傳入的資料會在當前幀尾廣播給所有註冊在此場景的連線 採用TCP(如果有建立TCP連線的話)
    fn tcp_frame_sync_broadcast(&mut self, data: dyn Any);
    /// 傳入的資料會在當前幀尾廣播給所有註冊在此場景的連線 採用UDP(如果有建立UDP連線的話)
    fn udp_frame_sync_broadcast(&mut self, data: dyn Any);
    /// 傳入的資料會在當前幀尾廣播給所有註冊在此場景的連線 採用KCP(如果有建立KCP連線的話)
    fn kcp_frame_sync_broadcast(&mut self, data: dyn Any);

    fn scene_obj(&mut self) -> Box<ScenesController<SCENE, GAMEOBJECT>>;
}

impl<SCENE, GAMEOBJECT> TScenesController<SCENE, GAMEOBJECT>
    for ScenesController<SCENE, GAMEOBJECT>
{
    fn create_gameobject(
        &mut self,
        obj: GAMEOBJECT,
    ) -> Box<dyn TGameObjectController<SCENE, GAMEOBJECT>> {
        todo!()
    }
    fn remove_gameobject(&mut self, obj: Box<dyn TGameObjectController<SCENE, GAMEOBJECT>>) {}

    fn mean(&self) -> f64 {
        todo!()
    }

    fn meanc(&self, f: &f64) -> f64 {
        todo!()
    }

    /// frame_sync皆為在幀尾組合成一個大的資料傳送
    fn tcp_frame_sync_broadcast(&mut self, data: dyn Any) {
        todo!()
    }

    fn udp_frame_sync_broadcast(&mut self, data: dyn Any) {
        todo!()
    }

    fn kcp_frame_sync_broadcast(&mut self, data: dyn Any) {
        todo!()
    }

    fn scene_obj(&mut self) -> SCENE {
        todo!()
    }

    fn frame_pres_set(&mut self,num:FrameNum) {
        // self.
        todo!()
    }

    fn tcp_send(&mut self, conn_id: ConnectionID, data: dyn Any) {
        todo!()
    }

    fn udp_send(&mut self, conn_id: ConnectionID, data: dyn Any) {
        todo!()
    }

    fn kcp_send(&mut self, conn_id: ConnectionID, data: dyn Any) {
        todo!()
    }

    fn tcp_broadcast(&mut self, data: dyn Any) {
        todo!()
    }

    fn udp_broadcast(&mut self, data: dyn Any) {
        todo!()
    }

    fn kcp_broadcast(&mut self, data: dyn Any) {
        todo!()
    }
}

pub struct ScenesController<SCENE, GAMEOBJECT> {
    scenes: Box<Scenes<SCENE, GAMEOBJECT>>,
    create_gameobject_queue: Vec<GAMEOBJECT>,
    tcp_frame_sync_broadcast_queue: Sender<dyn Any>,
    udp_frame_sync_broadcast_queue: Sender<dyn Any>,
    kcp_frame_sync_broadcast_queue: Sender<dyn Any>,
}

impl <S,G>ScenesController<S,G> {
    pub fn new()->BScenesController<S,G> {
        todo!()
    }
}
