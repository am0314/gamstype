use super::{game_object::GameObjectID, scenes_controller::{ScenesController, TScenesController}};

pub type BGameObjectController<SCENE, GAMEOBJECT> =
    Box<dyn TGameObjectController<SCENE, GAMEOBJECT>>;
pub trait TGameObjectController<SCENE, GAMEOBJECT> {
    fn Gameobject_id(&mut self) -> GameObjectID;
    fn Scenes(&mut self) -> Box<dyn TScenesController<SCENE, GAMEOBJECT>>;
    fn Destory(&mut self);
}

pub struct GameObjectController<SCENE, GAMEOBJECT> {
    scenes:Box<ScenesController<SCENE, GAMEOBJECT>>,
    gameobject_id: GameObjectID,
    scenes_controll: Box<ScenesController<SCENE, GAMEOBJECT>>,
    gameobject: Box<GAMEOBJECT>,
}

impl<SCENE, GAMEOBJECT> TGameObjectController<SCENE, GAMEOBJECT>
    for GameObjectController<SCENE, GAMEOBJECT>
{
    fn Gameobject_id(&mut self) -> GameObjectID {
        self.gameobject_id
    }

    fn Scenes(&mut self) -> Box<ScenesController<SCENE, GAMEOBJECT>> {
        self.scenes
    }

    fn Destory(&mut self) {
        todo!()
    }
}
