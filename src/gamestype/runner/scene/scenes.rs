// use crossbeam::channel::{after, select, tick};
use crossbeam::channel::{after, select, tick, Receiver, Sender};

use std::{
    any::Any,
    cell::RefCell,
    collections::HashMap,
    sync::{Arc, RwLock},
    time::{Duration, Instant},
};

use rayon::prelude::*;

use crate::gamestype::{
    connection::entity::entity::{ConnectionDataReceiver, ConnectionID},
    runner::service::{
        conf::Listen::ListeneType,
        runner::{GenericID, Runner},
    },
};

use super::{
    game_object::{BGameObject, GameObjectID, TGameObject},
    game_object_controller::TGameObjectController,
    scenes_controller::{self, BScenesController, TScenesController, AScenesController},
};

pub struct ScenesSet<SCENE> {
    /// max_frame_pre_s every second frame number
    frame_every_sec: FrameNum,

    /// 場景實現體
    scenes_impl: SCENE,
    // scenes_impl: Box<dyn TScenes>,
}
impl<SCENE,GAMEOBJECT> ScenesSet<SCENE>
where
    SCENE: TScenes<SCENE,GAMEOBJECT>,
{
    pub fn new(scenes_impl: SCENE, frame_every_sec: u32) -> Self {
        Self {
            frame_every_sec,
            scenes_impl,
        }
    }
}

pub type FrameNum = u32;

// ScenesBasic 實現場景基本功能
pub struct Scenes<SCENE, GAMEOBJECT> {
    // runner: Arc<Runner>,
    /// conf
    conf: ScenesSet<SCENE>,

    /// 場景物件池 物件必須通過對scenes註冊才能獲得 否則scene無法進行幀同步
    game_object_pool: HashMap<GameObjectID, Box<dyn TGameObject<SCENE, GAMEOBJECT>>>,

    // scenes_command_queue: Vec<Box<dyn ScenesCmd> >,
    /// mean 用於計算每幀乘數
    /// 例如50幀中 每秒移動10 = 每幀移動0.2
    /// 算式為10/50=0.2 同樣可用於計算場景幀間隔
    mean: f64,

    // 場景關閉信號欄位
    close_sign: (Sender<()>, Receiver<()>),
    frame_update_sign: (Sender<FrameNum>, Receiver<FrameNum>),

    /// 幀同步資料緩衝區
    frame_sync_data_queue: Vec<dyn Any>,

    broadcast_groub: HashMap<ConnectionID, ConnectionDataReceiver>,

    pub serialize: Box<dyn Fn(&dyn Any) -> Vec<u8>>,
    pub deserialize: Box<dyn Fn(&dyn Any, Vec<u8>)>,

    gameobject_create_queue: Vec<GAMEOBJECT>,
    controller:AScenesController<SCENE,GAMEOBJECT>,
}
/// 下游自己實現的場景邏輯
/// scene lifecycle:
///
/// scene.OnStart
/// (
/// scene.scene_controller_queue_exec >
/// scene.OnFrame >
/// scene.gameibject_frame_exec(gameobject.OnFrame) >
/// self.BroadcastQueueExec >
/// ) >
/// scene.OnFinal
pub trait TScenes<S, G> {
    fn OnStart(&mut self, ctx: AScenesController<S, G>);
    fn OnFinal(&mut self, ctx: AScenesController<S, G>);
}

impl<SCENE, GAMEOBJECT> Scenes<SCENE, GAMEOBJECT>
where
    SCENE: TScenes<SCENE, GAMEOBJECT>,
{
    pub fn new(
        sceneSet: ScenesSet<SCENE>,
        ser: Box<dyn Fn(&dyn Any) -> Vec<u8>>,
        dser: Box<dyn Fn(&dyn Any, Vec<u8>)>,
    ) -> Self {
        Self {
            conf: sceneSet,
            game_object_pool: HashMap::new(),
            mean: 1.0 / sceneSet.frame_every_sec as f64,
            frame_sync_data_queue: Vec::new(),
            close_sign: crossbeam::channel::unbounded(),
            frame_update_sign: crossbeam::channel::unbounded(),
            gameobject_create_queue: Vec::new(),
            broadcast_groub: HashMap::new(),
            serialize: ser,
            deserialize: dser,
            controller: todo!(),
        }
    }
    /// 處理場景活動
    fn scene_controller_queue_exec(&mut self) {
        todo!()
    }

    pub fn scenes_run(&mut self) {
        self.conf.scenes_impl.OnStart(self.controller);

        // 用於幀邏輯時間基準
        let ticker = tick(Duration::try_from_secs_f64(self.mean));

        loop {
            select! {
                recv(ticker) -> _ => {
                    self.gameibject_frame_exec()
                },
                // 收到幀率變化信號
                recv(self.frame_update_sign.1)->frame_num=>{
                    self.mean=1.0 / frame_num as f64;
                    let ticker = tick(Duration::try_from_secs_f64(self.mean));
                },
                // 收到關閉信號
                recv(self.close_sign.1)->_=>{
                    break;
                },
            }
            self.frame_sync_brodcast();
        }

        self.conf.scenes_impl.OnFinal();
    }

    /// 每幀對註冊於自己的gameobject執行一次frame
    fn gameibject_frame_exec(&mut self) {
        for (k, v) in &self.game_object_pool {
            v.OnFrame(&self);
        }
    }

    fn broadcast_queueExec(&mut self) {
        todo!()
    }

    fn frame_sync_brodcast(&mut self) {
        todo!()
        // self.frame_sync_data_queue
    }
}

impl<SCENE, GAMEOBJECT> TScenesController<SCENE, GAMEOBJECT> for Scenes<SCENE, GAMEOBJECT> {
    fn create_gameobject(
        &mut self,
        obj: GAMEOBJECT,
    ) -> Box<dyn TGameObjectController<SCENE, GAMEOBJECT>> {
        todo!()
    }

    fn mean(&self) -> f64 {
        self.mean
    }

    fn meanc(&self, f: &f64) -> f64 {
        self.mean * f
    }

    fn tcp_frame_sync_broadcast(&mut self, data: dyn Any) {
        todo!()
    }

    fn udp_frame_sync_broadcast(&mut self, data: dyn Any) {
        todo!()
    }

    fn kcp_frame_sync_broadcast(&mut self, data: dyn Any) {
        todo!()
    }

    fn scene_obj(&mut self) -> SCENE {
        self.conf
    }
}
