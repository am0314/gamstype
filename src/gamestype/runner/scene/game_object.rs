use std::sync::Arc;

use super::{scenes::{ Scenes}, game_object_controller::BGameObjectController};

pub type BGameObject<S,G> = Box<dyn TGameObject<S,G>>;
// TGameObject 只與Scene互動    
pub trait TGameObject<S,G> {
    fn OnSpawning(&mut self, object_ctx:  BGameObjectController<S,G>);
    fn OnFrame(&mut self, object_ctx:  BGameObjectController<S,G>);
    fn OnDestory(&mut self, object_ctx:  BGameObjectController<S,G>);
}






// pub type BGameObjectCTX = Box<dyn TGameObjectCTX + Sync + Send>;
// pub trait TGameObjectCTX {

//     fn syncDataToGround(&self,data: Vec<u8>);
// }

pub type GameObjectID = u32;
