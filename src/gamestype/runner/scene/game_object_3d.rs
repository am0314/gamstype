use std::any::Any;

use crate::gamestype::runner::unit::{transform_vec2F64::{Vec2F64}, transform_vec3F64::Vec3F64};





pub struct Object2d {
    p: Vec2F64,
    r: Vec2F64,
}

pub struct Object3d {
    p: Vec3F64,
    r: Vec3F64,
}

impl Object3d {
    pub fn zero_point() -> Self {
        Object3d {
            p: Vec3F64 { x: 0.0, y: 0.0, z: 0.0 },
            r: Vec3F64 { x: 0.0, y: 0.0, z: 0.0 },
        }
    }
    pub fn movie(offset: Vec3F64) {}
}

