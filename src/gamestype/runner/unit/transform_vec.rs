use std::any;



pub trait TransformVec {
    fn distance(&self,target:Self)->f64;
    fn add(&mut self,target:&Self);
    fn set_to(&mut self,target:&Self);
    
}