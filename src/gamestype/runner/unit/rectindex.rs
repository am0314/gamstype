// rectindex是用來計算和輸出填滿index的方法

/// Rect1Date export a rect index
/// # Panics
/// when any parameter is zero
/// # Examples
/// ```
/// let rect = Rect1Date(3,2,3);
/// println!("{:?}",rect);
/// /*
///  [0, 0, 1, 1, 2, 2, 0, 0, 1, 1, 2, 2, 0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 3, 3, 4, 4, 5, 5, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 6, 6, 7, 7, 8, 8, 6, 6, 7, 7, 8, 8]rect [0, 0, 0, 1, 1, 1, 2, 2, 2, 0, 0, 0, 1, 1, 1, 2, 2, 2, 0, 0, 0, 1, 1, 1, 2, 2, 2, 3, 3, 3, 4, 4, 4, 5, 5, 5, 3, 3, 3, 4, 4, 4, 5, 5, 5, 3, 3, 3, 4, 4, 4, 5, 5, 5, 6, 6, 6, 7, 7, 7, 8, 8, 8, 6, 6, 6, 7, 7, 7, 8, 8, 8, 6, 6, 6, 7, 7, 7, 8, 8, 8]
/// as
/// 0, 0, 1, 1, 2, 2,
/// 0, 0, 1, 1, 2, 2,
/// 0, 0, 1, 1, 2, 2,
/// 3, 3, 4, 4, 5, 5,
/// 3, 3, 4, 4, 5, 5,
/// 3, 3, 4, 4, 5, 5,
/// 6, 6, 7, 7, 8, 8,
/// 6, 6, 7, 7, 8, 8,
/// 6, 6, 7, 7, 8, 8
///  */
/// ```
pub fn Rect1Date(side_lenth: usize, w: usize, h: usize) -> Vec<usize> {
    if side_lenth == 0 || w == 0 || h == 0 {
        panic!("some parameter is zero")
    }
    let mut res = {
        let capacity = side_lenth.pow(2) * h * w;
        Vec::with_capacity(capacity)
    };

    for y in 0..side_lenth {
        let mut left_to_right_raw = Vec::new();
        for _ in 0..h {
            for x in 0..side_lenth {
                let mut raw = vec![x + (y * side_lenth); w];
                left_to_right_raw.append(&mut raw);
            }
        }
        res.append(&mut left_to_right_raw)
    }
    res
}
/// Rectangle1Date
/// like Rect1Date but can't custom size , only Rectangle rect
pub fn Rectangle1Date(side_lenth: usize, size: usize) -> Vec<usize> {
    if side_lenth == 0 || size == 0 {
        panic!("some parameter is zero")
    }
    let mut res = {
        let capacity = side_lenth.pow(2) * size.pow(2);
        Vec::with_capacity(capacity)
    };

    for y in 0..side_lenth {
        let mut left_to_right_raw = Vec::new();
        for _ in 0..size {
            for x in 0..side_lenth {
                let mut raw = vec![x + (y * side_lenth); size];
                left_to_right_raw.append(&mut raw);
            }
        }
        res.append(&mut left_to_right_raw)
    }
    res
}

// rectSquareStaggeredCoordinates 給出一個正方形交錯座標填充
pub fn rectSquareStaggeredCoordinates(side_lenth: usize, size: usize) {
    let lay1 = side_lenth.pow(2);
    let lay2 = (side_lenth - 1).pow(2);
}
