use std::{any, collections::HashMap, rc::Rc};

use crate::gamestype::connection::entity::entity::ConnectionID;

use super::{
    beacon::Beacon,
    rectindex,
    transform_vec2F64::{Vec2F64}, transform_vec3F64::Vec3F64,
};

#[derive(Clone)]
pub struct Grid {
    beacons: Vec<Rc<Beacon>>,
}
pub struct GridBoard3D {
    offset: GridOffset3D,
    grids: Vec<Vec<Vec<Grid>>>,
}
pub struct GridBoard2D {
    pub offset: Vec2F64,
    pub grids: Vec<Vec<Grid>>,
    pub beacon_map: HashMap<u32, Rc<Beacon>>,
}
pub struct GridOffset3D {
    x: f64,
    y: f64,
    z: f64,
}

/// GroundSyncsController 對ground註冊後返回的控制器特徵 
pub trait GroundSyncsController  {
    fn Destory(&self);
    fn Transfer(&self,newLand:GridBoard2D);
}

/// Grid  GridBoard單元
impl Grid {
    pub fn new() -> Self {
        Self {
            beacons: Vec::new(),
        }
    }
    pub fn beacon_register(&self, beacon: Rc<Beacon>) {
        self.beacons.push(beacon)
    }
    pub fn revice(&self, data: Vec<u8>) {
        for b in self.beacons {
            b.revice(data)
        }
    }
    /// revice_exclude 排除式發送資訊
    pub fn revice_exclude(&self, exclude: ConnectionID, data: Vec<u8>) {
        for b in self.beacons {
            b.revice_exclude(exclude, data)
        }
    }
}

impl GridBoard3D {
    pub fn new(size: Vec3F64) -> Self {
        let grids =
            vec![vec![vec![Grid::new(); size.z as usize]; size.y as usize]; size.x as usize];
        Self {
            grids,
            offset: GridOffset3D {
                x: size.x / 2.0,
                y: size.y / 2.0,
                z: size.z / 2.0,
            },
        }
    }
}

impl GridBoard2D {
    // new 純GridBoard2D
    pub fn new(size: Vec2F64) -> Self {
        let grids = vec![vec![Grid::new(); size.y as usize]; size.x as usize];
        Self {
            grids,
            offset: Vec2F64 {
                x: size.x / 2.0,
                y: size.y / 2.0,
            },
            beacon_map: HashMap::new(),
        }
    }
    /// newGroundWithBeaconSquare 正方形棋盤 有盲區
    pub fn newGroundWithBeaconSquare(beacon_side_length_num: usize, beacon_size: usize) -> Self {
        let size_x = beacon_side_length_num * beacon_size;
        let size_y = beacon_side_length_num * beacon_size;

        let grids = vec![vec![Grid::new(); size_x]; size_y];
        let beacons = vec![Rc::new(Beacon::new()); beacon_side_length_num.pow(2)];

        let rect_index = rectindex::Rectangle1Date(beacon_side_length_num, beacon_size).iter();
        for x in 0..grids.len() {
            for y in 0..grids[x].len() {
                let Some(rid) = rect_index.next();
                grids[x][y].beacon_register(beacons[*rid].clone())
            }
        }
        // 建立beacon列表
        let beacon_map = HashMap::new();
        let count = 0;
        for b in beacons {
            beacon_map.insert(count, b.clone());
            count += 1;
        }

        Self {
            grids,
            offset: Vec2F64 {
                x: size_x as f64 / 2.0,
                y: size_y as f64 / 2.0,
            },
            beacon_map: beacon_map,
        }
    }
    /// 正方形交錯式棋盤MMO視野
    /// beacon_num 表示大邊長的燈塔數
    /// beacon_size 表示每個燈塔的註冊格數
    /// # 範例
    /// # panic
    /// beacon_num或是beacon_size 任一小於2
    /// 設定值建議與前端遊戲世界座標吻合或更大
    pub fn newGroundWithBeaconSquareStaggered(beacon_num: usize, beacon_size: usize) -> Self {
        if beacon_num < 2 || beacon_size < 2 {
            panic!("beacon_num and beacon_size can't less then 2")
        }

        // main_size 主信標數
        let main_size = beacon_num * beacon_num;

        // 水平偏移信標總數 (x-1)*y
        let horizontal_size = (beacon_num - 1) * beacon_num;

        // 縱向偏移信標總數x*(y-1)
        let portrait_size = (beacon_num) * (beacon_num - 1);

        // 斜向信標總數 (x-1)*(y-1)
        let oblique_size = (beacon_num - 1) * (beacon_num - 1);

        // 生成所有格子
        let grids = vec![vec![Grid::new(); beacon_num * beacon_size]; beacon_num * beacon_size];

        // 生成所有信標
        let beacons = vec![
            Rc::new(Beacon::new());
            main_size + horizontal_size + portrait_size + oblique_size
        ];

        let rect_index = rectindex::Rectangle1Date(beacon_size, beacon_size).iter();

        let b_count = 0;
        for m in 0..main_size {
            for x in 0..beacon_size {
                for y in 0..beacon_size {
                    grids[(beacon_num * beacon_size) + x][(beacon_num * beacon_size) + y]
                        .beacon_register(beacons[b_count].clone())
                }
            }
            b_count += 1;
        }

        for m in 0..horizontal_size {
            for x in 0..beacon_size - 1 {
                for y in 0..beacon_size {
                    let x = ((beacon_num * beacon_size) + x) as f64;
                    x = x + 0.5;
                    let x = x as usize;
                    grids[x][(beacon_num * beacon_size) + y]
                        .beacon_register(beacons[b_count].clone())
                }
            }
            b_count += 1;
        }

        for m in 0..portrait_size {
            for x in 0..beacon_size {
                for y in 0..beacon_size - 1 {
                    let y = ((beacon_num * beacon_size) + y) as f64;
                    y = y + 0.5;
                    let y = y as usize;
                    grids[x][y].beacon_register(beacons[b_count].clone())
                }
            }
            b_count += 1;
        }

        for m in 0..oblique_size {
            for x in 0..beacon_size - 1 {
                for y in 0..beacon_size - 1 {
                    let x = ((beacon_num * beacon_size) + x) as f64;
                    x = x + 0.5;
                    let x = x as usize;
                    let y = ((beacon_num * beacon_size) + y) as f64;
                    y = y + 0.5;
                    let y = y as usize;
                    grids[x][y].beacon_register(beacons[b_count].clone())
                }
            }
            b_count += 1;
        }

        // 建立beacon列表
        let beacon_map = HashMap::new();
        let count = 0;
        for b in beacons {
            beacon_map.insert(count, b.clone());
            count += 1;
        }

        Self {
            grids,
            offset: Vec2F64 {
                x: beacon_size as f64 / 2.0,
                y: beacon_size as f64 / 2.0,
            },
            beacon_map: beacon_map,
        }
    }

    /// revice 根據v2座標 檢查該格子是否存在 並且發送接收資訊請求
    pub fn revice(&self, v2: &Vec2F64, data: Vec<u8>) {
        if let Some(yvec) = self.grids.get((v2.x + self.offset.x) as usize) {
            if let Some(g) = yvec.get((v2.y + self.offset.y) as usize) {
                g.revice(data)
            }
        }
    }
    //revice_exclude 同revice 不過是可排除式的
    pub fn revice_exclude(&self, v2: &Vec2F64, exclude: ConnectionID, data: Vec<u8>) {
        if let Some(yvec) = self.grids.get((v2.x + self.offset.x) as usize) {
            if let Some(g) = yvec.get((v2.y + self.offset.y) as usize) {
                g.revice_exclude(exclude, data)
            }
        }
    }



}
