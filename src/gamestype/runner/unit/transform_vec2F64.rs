
use super::transform_vec::TransformVec;
#[derive(Clone, Copy)]

pub struct Vec2F64 {
    pub x: f64,
    pub y: f64,
}
impl Vec2F64 {
    pub fn spawn_zeor() -> Self  {
        Self { x: 0.0, y: 0.0 }
    }
}
impl TransformVec for Vec2F64 {
    fn distance(&self, target: Self) -> f64 {
        let dx = (self.x - target.x).powf(2.0);
        let dy = (self.y - target.y).powf(2.0);
        let dr = dx + dy;
        dr.sqrt()
    }

    fn add(&mut self, target: &Self) {
        self.x += target.x;
        self.y += target.y;
    }

    fn set_to(&mut self, target: &Self) {
        self.x = target.x;
        self.y = target.y;    }
}

// impl Vec3 {
// ///distance 對另一目標點取得距離
// fn distance(&self, target: &Vec3) -> f64 {
//     let dx = (self.x - target.x).powf(2.0);
//     let dy = (self.y - target.y).powf(2.0);
//     let dz = (self.z - target.z).powf(2.0);
//     let dr = dx + dy + dz;
//     dr.sqrt()
// }
// fn move_offset(&self, offset: Vec3) {
//     self.x += offset.x;
//     self.y += offset.y;
//     self.z += offset.z;
// }
// fn move_offset_differential(&self, offset: Vec3) {
//     self.x += offset.x;
//     self.y += offset.y;
//     self.z += offset.z;
// }
// fn move_to(&self, x: f64, y: f64, z: f64) {
//     self.x = x;
//     self.y = y;
//     self.z = z;
// }
// fn look_at(&self, vec3: Vec3) {}
// }
