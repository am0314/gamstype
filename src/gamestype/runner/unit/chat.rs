use crate::gamestype::connection::entity::entity::{Entity, ConnectionID};
use rayon::prelude::*;
use std::sync::RwLock;
use std::{
    collections::HashMap,
    sync::{
        mpsc::{Receiver, Sender},
        Arc,
    },
};

pub struct MainChan {}

pub type RwSender = RwLock<HashMap<ConnectionID, Sender<Vec<u8>>>>;

/// Chat 會對所有註冊的頻道廣播資訊
#[derive(Clone)]
pub struct Chat {
    // subscriptions 所有對此頻道註冊
    subscriptions: Arc<RwSender>,
    // subscription: Arc<RwLock<HashMap<EntityID, Sender<Vec<u8>>>>>,
    // register : RwLock<HashMap<SocketAddr,Arc<Entity>>> ,
}

impl Chat {
    pub fn new() -> Self {
        Chat {
            subscriptions: Arc::new(RwLock::new(HashMap::new())),
        }
    }
    pub fn revice(&self, data: Vec<u8>) {
        if let Ok(sub) = self.subscriptions.read() {
            for senders in sub.iter() {
                senders.1.send(data);
            }
        };
    }
    pub fn revice_exclude(&self, exclude: ConnectionID, data: Vec<u8>) {
        if let Ok(sub) = self.subscriptions.read() {
            for senders in sub.iter().filter(|s| s.0 != &exclude) {
                senders.1.send(data);
            }
        };
    }
    /// subscription 訂閱
    pub fn subscription(&self, id: &ConnectionID, sender: &Sender<Vec<u8>>) {
        if let Ok(map) = self.subscriptions.write() {
            map.insert(*id, sender.clone());
        }
    }
    /// desubscription 取消訂閱
    pub fn desubscription(&self, id: &ConnectionID) {
        if let Ok(map) = self.subscriptions.write() {
            map.remove(id);
        }
    }
}
// map.par_iter().for_each(|(key, value)| {
//     thread::sleep(Duration::from_millis(1000));
// });
