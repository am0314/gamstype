use super::{transform_vec2F64::Vec2F64, transform_vec3F64::Vec3F64};


pub struct Transform3D {
    pub position: Vec3F64,
    pub ration: Vec3F64,
}

impl Transform3D {}

/// Transform2D 2D物件變換體 用以表達空間資訊
pub struct Transform2D {
    /// 空間變換包資訊
    pub pack: Transform2DataPack,
    /// differential 微分值 由scene設定 也可自行設定
    /// 用於移動或轉向的時候符合場景的幀數
    pub differential: f64,
}

#[derive(Clone, Copy)]
/// Transform2DataPack 2D空間變換量
pub struct Transform2DataPack {
    /// pos 點座標
    pub pos: Vec2F64,
    /// rot 方向
    pub rot: f64,
}

impl Transform2D {
    /// pack_sync 同步包資訊 
    pub fn pack_sync(&self, pack: Transform2DataPack) {
        self.pack = pack
    }

    /// distance 對另一目標點取得距離
    pub fn distance(&self, target: &Vec3F64) -> f64 {
        let dx = (self.pack.pos.x - target.x).powf(2.0);
        let dy = (self.pack.pos.y - target.y).powf(2.0);
        let dr = dx + dy;
        dr.sqrt()
    }
    /// move_offset 增量移動
    pub fn move_offset(&self, offset: Vec3F64) {
        self.pack.pos.x += offset.x;
        self.pack.pos.y += offset.y;
    }
    /// move_offset_differential 微分增量移動
    pub fn move_offset_differential(&self, offset: Vec3F64) {
        self.pack.pos.x += offset.x * self.differential;
        self.pack.pos.y += offset.y * self.differential;
    }

    /// move_portal_to 直接移動座標
    pub fn move_portal_to(&self, target: Vec3F64) {
        self.pack.pos.x = target.x;
        self.pack.pos.y = target.y;
    }

    /// trun 轉向
    pub fn trun(&self, offset: f64) {
        self.pack.rot = 360.0 % (self.pack.rot + offset)
    }
    /// trun 微分轉向
    pub fn trun_differential(&self, offset: f64) {
        self.pack.rot = 360.0 % (self.pack.rot + (offset * self.differential))
    }

    /// look_at 面向指定方向
    fn look_at(&self, target: f64) {
        self.pack.rot = 360.0 % target
    }
}
