use std::any;

use super::transform_vec::TransformVec;

#[derive(Clone, Copy)]
pub struct Vec3F64 {
    pub x: f64,
    pub y: f64,
    pub z: f64,
}

impl TransformVec for Vec3F64 {
    fn distance(&self,target:Self)->f64 {
        todo!()
    }

    fn add(&mut self,target:&Self) {
        todo!()
    }

    fn set_to(&mut self,target:&Self) {
        todo!()
    }
}
