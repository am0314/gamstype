use std::{sync::mpsc::Sender, rc::Rc};

use crate::gamestype::connection::entity::entity::ConnectionID;

use super::{chat::Chat, transform_vec2F64::{ Vec2F64}, ground_grid::GridBoard2D};
use rayon::prelude::*;

/// Beacon 用於在grid(地格)上分享資訊的渠道
/// 不同類型的Beacon會根據自身探知範圍註冊
/// 被註冊的地格會指向向其註冊的Beacon
/// 並且任何發往該Beacon的訊息都會同步到該Beacon所有註冊的地格上
/// Beacon全都是靜態的
#[derive(Clone)]
pub struct Beacon {
    message_chan: Chat,
}
impl Beacon {
    pub fn revice(&self, data: Vec<u8>) {
        self.message_chan.revice(data)
    }
    // revice_exclude 排除式傳輸
    pub fn revice_exclude(&self, exclude: ConnectionID, data: Vec<u8>) {
        self.message_chan.revice_exclude(exclude, data)
    }
    fn subscription(&self, id: &ConnectionID, sender: &Sender<Vec<u8>>) {
        self.message_chan.subscription(id, sender)
    }
    /// desubscription 取消訂閱
    pub fn desubscription(&self, id: &ConnectionID) {
        self.desubscription(id)
    }


    pub fn new() -> Self {
        Self {
            message_chan: Chat::new(),
        }
    }
}
