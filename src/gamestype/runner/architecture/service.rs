use std::any::Any;


pub type StageResult = Result<(), String>;

pub struct ServiceSet {
    pub service_name: &'static str,
    pub service_impl: BService,
}

pub type BService = Box<dyn TService>;
pub type BOUService<SETCONF, CONNCTX, REVICEAC> =
    Box<dyn OverupTService<SETCONF, CONNCTX, REVICEAC>>;

pub trait ReviceTService {
    fn start(&mut self);
    fn shotdown(&mut self);
}
pub trait TService {
    fn start(&mut self);
    fn shotdown(&mut self);
}

pub trait OverupTService<SETCONF, CONNCTX, REVICEAC>
where
    REVICEAC: Clone,
{
    fn start(&mut self);
    fn shotdown(&mut self);

    // overup
    fn offset(&mut self) -> u8;
    fn set_offset(&mut self, offset: u8);
    fn call_fn(&mut self, cmd: Vec<u8>, ctx: &dyn Any);
    // fn call_fn(&mut self,cmd:&[u8],ctx:BOUContext<SETCONF, CONNCTX, REVICEAC>,ac:REVICEAC);
}

/*
是 RefCell 就像一個普通的指針，你可以隨時寫東西
有沒有需要 refcell 的例子？

你可以藉用一個指向內部值的可變指針 from.a &RefCell
但如果你這樣做兩次，那就是恐慌
（一次兩次）
它基本上是當前借用檢查器規則的運行時實現


編譯器是否/可以檢查互斥鎖是否會發生死鎖
*/
