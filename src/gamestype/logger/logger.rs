use std::sync::Arc;

pub struct LoggerConf {
    enable_level: Box<[Level]>,
    fields: Box<[Fields]>,
}

impl LoggerConf {}

pub enum Level {
    All,
    Track,
    Info,
    Attention,
    Warn,
    Error,
    Panic,
    Fatal,
}

pub enum Fields {
    Thread,
    File,
    Row,
    LogLevel,
    Time,
    Msg,
}
pub struct SimpleLogger;
impl SimpleLogger {
    pub fn new() -> Box<dyn TLogger> {
        Box::new(Self {})
    }
}

impl TLogger for SimpleLogger {
    fn track_0(&self, msg: String) {
        println!("[track]{}", msg);
    }

    fn info_5(&self, msg: String) {
        println!("[info_5]{}", msg);
    }

    fn attention_10(&self, msg: String) {
        println!("[attention_10]{}", msg);
    }

    fn warn_15(&self, msg: String) {
        println!("[warn_15]{}", msg);
    }

    fn error_20(&self, msg: String) {
        println!("[error_20]{}", msg);
    }

    fn panic_25(&self, msg: String) {
        println!("[panic_25]{}", msg);
    }

    fn fatal_30(&self, msg: String) {
        println!("[fatal_30]{}", msg);
    }
}
pub type ALogger = Arc<Box<dyn TLogger>>;
pub trait TLogger {
    fn track_0(&self, msg: String);
    fn info_5(&self, msg: String);
    fn attention_10(&self, msg: String);
    fn warn_15(&self, msg: String);
    fn error_20(&self, msg: String);
    fn panic_25(&self, msg: String);
    fn fatal_30(&self, msg: String);
}
