use std::sync::mpsc::Sender;
use std::sync::Mutex;

use futures::SinkExt;
pub const buffer_size: usize = 2048;

pub struct BufferSign {
    cut: usize,
    buf: [u8; buffer_size],
}

pub struct Buffer {
    read_stream: [u8; buffer_size],
    copy_buffer: CBuffer,
}

pub struct CBuffer {
    copy_buffer: Mutex<[u8; buffer_size]>,
}

impl CBuffer {
    pub fn DoSend(&self, cut: usize) -> Box<&[u8]> {
        let a = &self.copy_buffer.lock();
        // let a = a.unwrap();
        // let na = &a[..cut];
        // let na = Box::new(na);
        todo!()
        // return na;
        // &a[..cut]
    }
}

impl Buffer {
    pub fn new() -> Self {
        Self {
            read_stream: [0; buffer_size],
            copy_buffer: todo!(),
            // copy_buffer: ,
        }
    }
}

impl Default for Buffer {
    fn default() -> Self {
        Self::new()
    }
}
