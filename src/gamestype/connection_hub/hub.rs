use std::{collections::HashMap, sync::RwLock};

use crossbeam::{channel::{Receiver, Sender}, select};

use super::chat_pod::*;

struct Hub {
    pod_pool: HashMap<u64, BChatPod>,
    conn_pool: HashMap<u64, BChatPod>,
    brodcastChan: (Sender<[u8]>, Receiver<[u8]>),
}

impl Hub {
    pub fn Run(&mut self, addr: String, port: u32) {
        tokio::spawn(|| loop {
            select! {
                
            }
        })
    }
}

impl IHub for Hub {
    fn brodcast(&mut self, data: [u8]) {
        todo!()
    }

    fn pod_create(&mut self) -> Result<BChatPod, String> {
        todo!()
    }

    fn pod_remove(&mut self, pod: BChatPod) {
        todo!()
    }

    fn pod_list(&mut self) {
        todo!()
    }
}

trait IHub {
    fn brodcast(&mut self, data: [u8]);
    fn pod_create(&mut self) -> Result<BChatPod, String>;
    fn pod_remove(&mut self, pod: BChatPod);
    fn pod_list(&mut self);
}
