
pub mod manager {
    use std::collections::HashMap;
    use std::sync::{Arc,RwLock};

    use tokio::net::TcpStream;

    pub struct ConnectionManager {
        connMap: Arc<std::sync::RwLock<HashMap<String, ConnectionEntity>>> ,
        // locker:RwLock,
    }
    impl ConnectionManager {
        pub fn new() -> Self {
            // let a =RwLock::new(t)
            return ConnectionManager {
                connMap: Arc::new(RwLock::new(HashMap::new())) ,
            };
        }
        // pub fn Join(&mut self, conn_addr: String, conn: TcpStream) {
        //     self.connMap.insert(conn_addr, ConnectionEntity::new(conn));
        // }

        // pub fn leave(&mut self, conn_addr: String){
        //     self.connMap.remove(&conn_addr);
        // }

        // pub fn Get(&mut self, conn_addr: String){
        //     self.connMap.remove(&conn_addr);
        // }    
    }

impl Default for ConnectionManager {
    fn default() -> Self {
        Self::new()
    }
}

    pub struct ConnectionEntity {
        tcp_stream: TcpStream,
    }
    impl ConnectionEntity {
        pub fn new(tcp_stream: TcpStream) -> Self {
            return Self { tcp_stream };
        }
    }
}
