use std::{any::Any, collections::HashMap, io::Error, net::IpAddr};

use tokio::io::AsyncWriteExt;

use crate::gamestype::{
    connection::entity::entity,
    consts::buffer::buffer_size,
    logger::logger::{self, ALogger, TLogger},
    runner::{
        scene::{scenes::{ Scenes}, scenes_controller::ScenesController},
        service::{
            runner::{ Runner},
        },
    },
};

pub struct Context<CONNCTX, REVICEAC> {
    // revice_ac 用於自己在此service反序列化時使用的結構體
    pub revice_ac: REVICEAC,
    // sub_context 用於每條連線存放自己位於這個server的資料
    pub sub_context: CONNCTX,
    // logger的實現
    pub logger: ALogger,

    pub payload: Vec<u8>,
    pub message_len: u32,
}

type SendJobReslut = Result<(), String>;



pub type BContext<SETCONF, CONNCTX, REVICEAC> =
    Box<dyn TContext<SETCONF, CONNCTX, REVICEAC> + Sync + Send + 'static>;


pub type Subscription = u32;

// pub type BOUContext<SETCONF, ENTITY, REVICEAC> =
//     Box<dyn TOverupContext<SETCONF, ENTITY, REVICEAC> + Sync + Send + 'static>;
pub trait TContext<SETCONF, CONNCTX, REVICEAC> 
where REVICEAC:Clone{
    fn context(&mut self) -> CONNCTX;
    fn revice_ac(&mut self)->REVICEAC;
    fn addr(&mut self) -> IpAddr;
    fn tcp_send(&mut self, data: Box<dyn Any>);
    fn udp_send(&mut self, data: Box<dyn Any>);
    fn conn_close(&mut self);

    fn chan_make(&mut self, s: Subscription);
    fn chan_join(&mut self, s: Subscription);
    fn chan_close(&mut self, s: Subscription);

    // subscribed show list of already subscribed chan
    fn chan_subscribed_list(&mut self);

    // subscribeable_list list
    fn chan_subscribeable_list(&mut self);
    fn chan_subscription(&mut self, s: Subscription);
    fn chan_unsubscribe(&mut self, s: Subscription);
}

pub type BOverupContext<CONFSET, CONNCTX, REVICEAC> = Box<dyn TOverupContext<CONFSET, CONNCTX, REVICEAC>>;
/// CONNCTX 代表連線
pub trait TOverupContext<CONFSET, CONNCTX, REVICEAC> 
where REVICEAC:Clone{
    fn entity(&mut self) -> CONNCTX;
    fn revice_ac(&mut self)->REVICEAC;
    fn logger(&mut self) -> ALogger;
    fn addr(&mut self) -> IpAddr;
    fn tcp_send(&mut self, data: Box<dyn Any>);
    fn udp_send(&mut self, data: Box<dyn Any>);
    fn kcp_send(&mut self, data: Box<dyn Any>);
    fn conn_close(&mut self);

    // fn udp_scene_sync_frame(&mut self, data: Box<dyn Any>);
    // fn tcp_scene_sync_frame(&mut self, data: Box<dyn Any>);
    // fn kcp_scene_sync_frame(&mut self, data: Box<dyn Any>);

    fn scenes_subscribed(&mut self,scenes:ScenesController);
    fn scenes_unsubscribed(&mut self,scenes:ScenesController);

    fn chan_make(&mut self, s: Subscription);
    fn chan_join(&mut self, s: Subscription);
    fn chan_close(&mut self, s: Subscription);

    // subscribed show list of already subscribed chan
    fn chan_subscribed_list(&mut self);

    // subscribeable_list list
    fn chan_subscribeable_list(&mut self);
    fn chan_subscription(&mut self, s: Subscription);
    fn chan_unsubscribe(&mut self, s: Subscription);

    // overup
    fn serialization_self(&mut self)->Result<(),&str>;
}

pub trait HandleList {
    fn HandleList(&self) -> HashMap<u8, String>;
}
