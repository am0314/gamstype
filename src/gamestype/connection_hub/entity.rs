pub mod entity {

    use futures::{io::Empty, select};
    use std::{
        cell::RefCell,
        collections::HashMap,
        error::Error,
        net::SocketAddr,
        rc::Rc,
        result,
        sync::{
            mpsc::{channel, Receiver, Sender},
            Arc, Mutex, RwLock,
        },
    };
    use tokio::{
        io::{AsyncReadExt, AsyncWriteExt},
        net::{TcpStream, UdpSocket},
    };

    use crate::gamestype::{
        connection::context::{BContext, Context},
        consts::buffer,

    };

    pub type ConnectionID = u64;
    pub struct Entity<'a, SETCONF, CONNCTX, REVICEAC>
    where
        REVICEAC: Clone,
    {
        pub entity_id: ConnectionID,
        pub addr: SocketAddr,
        pub upd: UDP<'a>,
        pub tcp: TCP<'a>,
        pub RC: Arc<dyn TEntityRC>,
        pub context: Vec<BContext<SETCONF, CONNCTX, REVICEAC>>,
        pub revice_sender:Sender<Vec<u8>>,
        pub revice_ch: ConnectionDataReceiver,
        pub revice_portocal_ac: REVICEAC,
        // pub HeaderMatch: HashMap<u8, &'a BOUService<SETCONF, CONNCTX, REVICEAC>>,
    }

    pub type ConnectionDataReceiver= Receiver<Vec<u8>>;

    // EntityRC Entity remote control
    pub trait TEntityRC {
        fn Close(&self);
    }
    struct EntityRC;
    impl TEntityRC for EntityRC {
        fn Close(&self) {
            todo!()
        }
    }
    impl EntityRC {
        pub fn new() -> Arc<Self> {
            let erc = Self {};
            Arc::new(erc)
        }
    }

    pub struct UDP<'a> {
        pub stream: UdpSocket,
        pub c2s_buffer: buffer::Buffer,
        pub c2s_revice: Sender<&'a [u8]>,
    }

    pub struct TCP<'a> {
        pub stream: TcpStream,
        pub c2s_buffer: buffer::Buffer,
        pub c2s_revice: Sender<&'a [u8]>,
    }

    impl<'a> TCP<'a> {
        pub fn new(stream: TcpStream) -> (Self, Receiver<buffer::Buffer>) {
            todo!()
        }

        pub async fn c2s_read(&'a mut self) {
            // let buf = self.c2s_buffer[..].as_mut();
            // let bufer_copy = Mutex::new(buffer::Buffer());

            // loop {
            //     let a = self.stream.read(buf).await;
            //     let a = match a {
            //         Ok(a) => a,
            //         Err(_) => return,
            //     };
            //     bufer_copy = buf.copy();
            //     self.c2s_revice.send(&buf[..a]);
            // }
        }
    }

    impl UDP<'static> {
        pub async fn new(
            addr: SocketAddr,
        ) -> Result<(Self, Receiver<buffer::Buffer>), Box<dyn Error>> {
            // let (s, r) = channel();
            // let us = UdpSocket::bind(addr).await?;

            // let n = Self {
            //     stream: us,
            //     c2s_buffer: buffer::Buffer(),
            //     c2s_revice: s,
            // };
            // return Ok((n, r));
            todo!()
        }
    }

    pub enum S2CCase {
        UDPSend,
        TCPSend,
        Close,
    }

    impl<SETCONF, REVICEAC, CONNCTX> Entity<'static, SETCONF, REVICEAC, CONNCTX>
    where
        REVICEAC: Clone,
        CONNCTX: Clone,
    {
        pub async fn new_tcp_conn(
            addr: SocketAddr,
            stream: TcpStream,
            ac: REVICEAC,
            ectx: CONNCTX,
        ) -> Result<(SocketAddr, Arc<dyn TEntityRC>), Box<dyn Error>> {
            let (udp, udata) = UDP::new(addr.clone()).await?;
            let (tcp, tdata) = TCP::new(stream);

            let (send, revic) = channel();

            // let e = Entity {
            //     addr,
            //     upd: udp,
            //     tcp: tcp,
            //     runner: todo!(),
            //     RC: EntityRC::new(),
            //     context: todo!(),
            //     revice_ch: revic,
            //     revice_portocal_ac: ac,
            //     HeaderMatch: HashMap::new(),
            // };
            todo!();

            // let e = Rc::new(e);
            // let e=rw
            // tokio::join!(e.tcp_c2s(), e.udp_c2s());

            // let (c2s_message,s2c_message)=channel();

            // return Ok((addr, e.RC.clone()));
        }

        // revice 接收cmd
        async fn revice(&mut self) {
            for mut data in &self.revice_ch {
                let ctx_index = self.runner.offset_index(data[0]);
                match ctx_index {
                    Err(e) => (),
                    Ok(ctx_index) => {
                        // 知道屬於哪個ctx後要再減去篇移值
                        let ctx = &mut self.context[ctx_index];
                        // 修改偏移值
                        data[0] = data[0] - ctx.service().offset();
                        // 修改偏移值接著往後送
                        ctx.service().call_fn(data, ctx)
                    }
                };

                // let ctx_index = self.runner.index([data[0] as usize]);
                // let ctx = self.context[ctx_index];
                // ctx.service.call(data, ctx.ac)
            }
        }

        async fn stream_fork(&mut self) {
            todo!()
            // let a=&self.tcp_c2s();
            // let b=&self.tcp_c2s();

            // tokio::join!(self.tcp_c2s(), self.udp_c2s());
        }

        // tcp_c2s when stream close , tcp_c2s also will return
        async fn tcp_c2s(stream: TcpStream, c2s_tcp_message: Sender<&[u8]>) {
            loop {
                // let res = &stream.read(&mut self.c2s_buffer[..]).await;
                // match res {
                //     Ok(us) => self.c2s_buffer[0..*us].to_vec(),
                //     _ => return,
                // };
            }
        }

        async fn tcp_s2c(&mut self) {
            // let a= &self.stream.write(&self.s2c_buffer).await;
            todo!();
        }

        async fn udp_c2s(stream: UdpSocket, c2s_udp_message: Sender<&[u8]>) {
            todo!()
        }

        async fn udp_s2c(&mut self) {
            // &self.udp_stream.send(buf)
            todo!()
        }
    }

    pub trait EntityCTX  {
        fn OnConnection(&mut self) {
            
        }
        fn OnDisConnection(&mut self) {
            
        }
    }
}
