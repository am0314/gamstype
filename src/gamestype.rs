pub mod connection_hub {
    pub mod hub;
    pub mod chat_pod;
    pub mod connection;
    pub mod manager;
}
pub mod consts {
    pub mod buffer;
    pub mod err;
}
pub mod logger {
    pub mod logger;
}

pub mod runner{
    pub mod architecture{
        // pub mod architecture;
    }
    pub mod scene{
        pub mod game_object_3d;
        pub mod game_object_2d;
        pub mod game_object;
        pub mod scenes_controller;
        pub mod scenes;
        pub mod game_object_controller;
    }
    pub mod service{
        // pub mod service;
        pub mod conf;
        pub mod flow;
        pub mod route_register;
        pub mod runner;
        pub mod scenes_manager;
        // pub mod service;
        pub mod stream;
        pub mod tcp;
        pub mod udp;
        pub mod default_serialize;
    }
    pub mod unit {
        pub mod beacon;
        pub mod ground_grid;
        pub mod chat;
        pub mod transform_vec;
        pub mod transform_vec2F64;
        pub mod transform_vec3F64;
        pub mod transform;
        pub mod rectindex;

    }
}

// pub mod connection {
//     pub mod chans;
//     pub mod context;
//     pub mod entity;
//     pub mod manager;
// }

// pub mod conf;
// pub mod flow;
// pub mod game_object;
// pub mod game_object_rientation;
// pub(crate) mod route_register;
// pub mod runner;
// pub mod scenes;
// pub mod scenes_manager;
// pub mod service;
// pub mod stream;
// pub mod tcp;
// pub mod udp;

// pub mod consts {
//     pub mod buffer;
//     pub mod err;
// }
// pub mod logger {
//     pub mod logger;
// }
