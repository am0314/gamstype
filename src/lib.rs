mod gamestype;
mod example;
// use crate::service::service::service::ServiceContainer;
// mod example;


use std::sync::{Arc, RwLock};
use gamestype::runner::service::runner::RunnerSetting;
// use tokio::io::{AsyncReadExt, AsyncWriteExt};
use tokio::net::TcpListener;

use crate::gamestype::runner::service::runner::Runner;


pub type StageResult = Result<(), String>;
// run_service service main start
pub async fn run_service<CONF, CONNCTX>(s: RunnerSetting<CONF>) 
// where REVICEPROTOCAL:Clone,CONNCTX:Clone,
{
    

    let mut s = Runner::new(s);
    for mut k in s.set.service_impl{
        k.service_impl.start();
    }
    // match s.service_container.service_impl.start() {
    //     Err(e) => panic!("{:?}", e),
    //     _ => (),
    // }



    let listener = TcpListener::bind(
        s.set.basic_config.tcp_addr,
    );

    // let arc_service = Arc::new(RwLock::new(s));
    // let listener = TcpListener::bind(
    //     arc_service
    //         .read()
    //         .unwrap()
    //         .service_container
    //         .basic_config
    //         .tcp_addr,
    // );
    let tokio_tcp_listener = listener.await.unwrap();

    loop {
        // let arc_service_clone = Arc::clone(&arc_service);
        let (stream, addr) = tokio_tcp_listener.accept().await.unwrap();
        // let e = arc_service_clone.write().unwrap().pool_inster(addr, stream);
        // tokio::join!(c2s(e.clone()), s2c(e));
    }
}



// async fn c2s(e: Arc<RwLock<entity::entity::Entity>>) {
//     let mut e_w = e.write().unwrap();
//     // let stream = &mut e_w.stream;
//     // let read_buffer = &mut e_w.read_buffer;

//     //    let r= e_w.tcp_c2s().await;
//     //    match r {
//     //     Ok(d) => {},
//     //     Err(_) => {

//     //     },
//     // }
// }

// async fn s2c(e: Arc<RwLock<entity::entity::Entity>>) {}
