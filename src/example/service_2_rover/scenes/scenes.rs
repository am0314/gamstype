use std::any::Any;

use crate::gamestype::runner::{unit::chat::Chat, scene::scenes::TScenes};

use super::game_object::wandering_thing_manager::WanderingThingManger;


pub struct RoverScenes {
    ///main_chan 主聊天室頻道
    main_chan: Chat,
    // 遊蕩物管理員
    Manger:WanderingThingManger,
    

    // 地格化訊息頻道
    // ground: GridBoard2D,
}
impl RoverScenes {
    pub fn new() -> RoverScenes {
        Self {
            main_chan: Chat::new(),
            Manger: todo!(),
        }
    }
}

impl Default for RoverScenes {
    fn default() -> Self {
        Self::new()
    }
}

impl TScenes for RoverScenes {
    fn OnStart(&mut self) {
        todo!()
    }

    fn OnFinal(&mut self) {
        todo!()
    }
}

struct ScenesCmd {
    cmd: u32,
    gid: u32,
    data: dyn Any,
}
