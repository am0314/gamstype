use crate::gamestype::runner::{
    scene::{
        game_object::TGameObject,
        game_object_3d::{self},
    },
    unit::{transform_vec2F64::Vec2F64, transform_vec::TransformVec},
};
use rand::prelude::*;

/// NOTE
/// 第一階段實現只要產生一堆可追蹤的WanderingThing 在scenes上跑來跑去即可
pub struct WanderingThing {
    // postition 當前座標
    postition: Vec2F64,
    // direction 每隔3秒改變一次遊蕩方向
    direction: Vec2F64,
    speed: Vec2F64,
    timeCount: f64,
}

impl WanderingThing {
    fn  OnDamage() {
        
    }
}

// impl <SCENE, GAMEOBJECT>TGameObject<SCENE, GAMEOBJECT> for WanderingThing {
//     fn OnSpawning(&mut self, sctx: BScenesController) {}

//     fn Frame(&mut self, sctx: BScenesController) {
//         self.timeCount += sctx.mean();
//         if self.timeCount > 3.0 {
//             self.timeCount = 0.0;
//             let mut rng = rand::thread_rng();
//             self.direction.x = rng.gen_range(0.0..10.0);
//             self.direction.y = rng.gen_range(0.0..10.0);
//         }
//         let zero_point=Vec2F64::spawn_zeor();
//         if self.postition.distance(zero_point)>100{
//             self.postition.set_to(zero_point);
//         }
//         // if self.postition

//         self.postition.add(&self.direction)
//     }

//     fn OnDestory(&mut self, sctx: BScenesController) {}
//     // fn Frame(&mut self, sctx: BScenesContext) {

//     // }
// }
