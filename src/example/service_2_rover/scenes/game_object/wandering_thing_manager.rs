use std::collections::HashMap;

use crate::gamestype::runner::{
    scene::{game_object::TGameObject, game_object_controller::BGameObjectController},
    unit::transform_vec2F64::Vec2F64,
};

#[derive(Clone)]
pub struct WanderingThingManger {
    // pub WTs:HashMap<>,
}
#[derive(Clone)]

pub struct SyncData {
    id: u32,
    post: Vec2F64,
}

impl<S,G> TGameObject<S,G> for WanderingThingManger {
    fn OnSpawning(&mut self, sctx: BGameObjectController<S,G>) {
        todo!()
    }

    fn OnFrame(&mut self, sctx: BGameObjectController<S,G>) {
        todo!()
    }

    fn OnDestory(&mut self, sctx: BGameObjectController<S,G>) {
        todo!()
    }
}
