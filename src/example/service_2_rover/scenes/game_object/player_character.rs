use std::any::Any;

use crate::gamestype::{runner::{
    unit::transform_vec2F64::{Vec2F64,}, scene::{game_object::{TGameObject}, game_object_controller::BGameObjectController},
}, self};


#[derive(Clone)]
pub struct PlayerCharacter {
    pub sync_data: SyncData,
}
#[derive(Clone)]

pub struct SyncData {
    id: u32,
    post: Vec2F64,
}

impl <SCENE,GAMEOBJECT>TGameObject<SCENE,GAMEOBJECT> for PlayerCharacter {
    fn OnSpawning(&mut self, object_ctx:  BGameObjectController<SCENE,GAMEOBJECT>) {
        todo!()
    }

    fn OnFrame(&mut self, object_ctx:  BGameObjectController<SCENE,GAMEOBJECT>) {
        let sc=object_ctx.Scenes();
    }

    fn OnDestory(&mut self, object_ctx:  BGameObjectController<SCENE,GAMEOBJECT>) {
        todo!()
    }
}
