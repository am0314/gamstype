use crate::gamestype::runner::scene::scenes::Scenes;

use super::scenes::game_object::player_character::PlayerCharacter;


#[derive(Clone)]
pub struct ConnContext {
    pub IsLogin:bool,
    pub memberID:u32,
    pub MyRole:PlayerCharacter,
    // pub CurrentScene:Scenes<RoverScenes,G>
}
