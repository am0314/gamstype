use std::sync::Arc;

#[derive(Default, Clone)]
pub struct Register {
    set: AccountSet,
    Name: String,
}
#[derive(Default, Clone)]

pub struct Login {
    set: AccountSet ,
}
#[derive(Default, Clone)]
struct AccountSet {
    Account: String,
    Password: String,
}
#[derive(Default, Clone)]

pub struct JoinRoom {
    RoomID: u32,
}
#[derive(Default, Clone)]

pub struct LeaveRoom {}
#[derive(Default, Clone)]

pub struct Mora {
    mora: u8,
}
enum EMora {
    // 剪刀
    Scissors,
    // 石頭
    Rock,
    // 布
    Fabric,
}

impl Default for EMora {
    fn default() -> Self {
        EMora::Scissors
    }
}

pub struct GameResult {
    Winner: String,
    p1: GameInfo,
    p2: GameInfo,
}
struct GameInfo {
    PlayerName: String,
    MoraType: EMora,
}
