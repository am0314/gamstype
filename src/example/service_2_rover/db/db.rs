extern crate diesel;
use diesel::prelude::*;

use diesel::sqlite::SqliteConnection;

use super::member::*;

pub struct DBConn {
    conn: SqliteConnection,
}
pub type DBRes = Result<(), &'static str>;
impl DBConn {
    pub fn new() -> Self {
        let conn = SqliteConnection::establish("sqlite/data")
            .unwrap_or_else(|_| panic!("Error connecting to {}", "sqlite/data"));
        DBConn { conn }
    }

    // fn Init(&mut self) -> DBRes {
    //     self.conn = SqliteConnection::establish("sqlite/data")
    //         .unwrap_or_else(|_| panic!("Error connecting to {}", "sqlite/data"));
    //     Ok(())
    // }
    // fn Login(&mut self, data: Login) -> DBRes {
    //     Ok(())
    // }
    // fn Register(&mut self, data: Login) -> DBRes {
    //     // diesel::insert_into(posts::table)
    //     //     .values(&data)
    //     //     .get_result(self.conn)
    //     //     .expect("Error saving new post")
    //     Ok(())
    // }
    // fn CoinChange(&mut self, member: Members, change: i32) -> DBRes {
    //     Ok(())
    // }
}
