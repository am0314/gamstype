use crate::{
    gamestype::runner::{
        scene::{scenes::{Scenes, ScenesSet}, scenes_controller::TScenesController},
        unit::transform_vec2F64::Vec2F64,
    },
    make_context_routes,
};

use super::{context::ConnContext, db::db, protocal::*};

pub struct ExampleServiceContext1<RoverScenes, G> {
    pub db: db::DBConn,
    // main_world 遊戲主場景
    pub main_world: dyn TScenesController<RoverScenes, G>,
}
#[derive(Debug)]

pub struct GameRoom {
    member: HashMap<u32, ()>,
}

impl<S, G> ExampleServiceContext1<S, G> {
    pub fn new() -> Box<Self> {
        let s = Self {
            db: db::DBConn::new(),
            main_world: Box::new(S::new()),
        };
        Box::new(s)
    }
    pub fn init(&self) {
        self.db = db::DBConn::new();
    }
}

struct TheRunner {}
struct TheRunnerConf {}
// service context設計點 相當於connection傳送封包時的header判斷
make_context_routes! {
    TheRunner
    ConnContext
    // 以下的self指service ctx指連線ctx 透過entity取得客製資料
    // Register 註冊帳號密碼
    fn Register(&mut self,ctx,data:&Register){

    }
    // Login 登入後自動生成新角色
    fn Login(&mut self,ctx,data:&LeaveRoom){
        if ctx.entity().IsLogin{
           ctx.tcp_send(RespError{ Msg: "already login" });
            return;
        };
        ctx.entity().IsLogin=true;
    }

    fn Join(&mut self,ctx,data:&ReqJoinGameScene){

    }

    fn Move(&mut self,ctx,data:&ReqMove){
        // note udp_scene_sync_frame 不應該在連線上就能存取 應該在gameobject才能使用
        // ctx.udp_scene_sync_frame(data)
        ctx.entity()
    }

    fn HitWT(&mut self,ctx,data:&HitWTReq){

    }



}

struct RespError {
    Msg: String,
}

struct ReqMove {
    pos: Vec2F64,
}

struct HitWTReq {
    WTID: u32,
}

struct ReqJoinGameScene {
    ScenesID: u32,
}
