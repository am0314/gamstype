pub mod conf;
// pub mod service_1{
//     pub mod scenes;
//     pub mod context;
//     pub mod protocal;
//     pub mod object;
//     pub mod service;
//     pub mod db{
//         pub mod db;
//         pub mod member;
//     }
// }
pub mod service_2_rover{
    pub mod scenes{
        pub mod scenes;
        pub mod game_object{
            pub mod player_character;
            pub mod wandering_thing;
            pub mod wandering_thing_manager;
        }
    }
    pub mod context;
    pub mod protocal;

    pub mod service;
    pub mod service_game;
    pub mod db{
        pub mod db;
        pub mod member;
    }
}

pub mod example;
