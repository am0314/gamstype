use std::fmt::Error;

// use crate::example::service_1::{context, protocal::*};
use crate::gamestype::runner::service::conf::{ListenConfig, Listen};
use crate::gamestype::runner::service::runner::RunnerSetting;
use crate::run_service;

use super::{conf::{ ExampleConf}, service_2_rover::service::ExampleServiceContext1};
// use super::service_1;
// use super::service_1::service::ExampleServiceContext1;
// use super::service_2::service::ExampleService2;

// use service::{TService,StageResult};

async fn example_main() {
    let a = ExampleServiceContext1 {
        db: todo!(),
        main_world: todo!(),
    };

    let mut runner_set = RunnerSetting {
        basic_config: ListenConfig {
            listens: vec![Listen::tcp("0.0.0.0:25963"),Listen::kcp("0.0.0.0:25963")],
        },
        customized_config: ExampleConf::new(),
        serialize: None,
        logger: None,
    };
    
    run_service(runner_set).await;
}
